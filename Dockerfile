# Use the official PHP 8.1 FPM Debian-based image as the base
FROM php:8.0-fpm

# Set working directory
WORKDIR /var/www/html

# Update the package repository
RUN apt-get update

# Install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    nano \
    bash \
    curl \
    zip \
    unzip \
    npm \
    nodejs \
    supervisor \
    zlib1g-dev \
    libzip-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libicu-dev \
    libxml2-dev \
    libonig-dev \
    ${PHPIZE_DEPS}

# Install and configure PHP extensions
RUN docker-php-ext-install \
    exif \
    mysqli \
    pdo \
    pdo_mysql \
    zip \
    pcntl \
    gd \
    opcache \
    intl \
    mbstring \
    xml \
    ctype \
    bcmath

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Create laravel caching folders
RUN mkdir -p /var/www/html/storage/logs
RUN mkdir -p /var/www/html/bootstrap/cache

# Fix files ownership
RUN chown -R www-data:www-data \
    /var/www/html/storage \
    /var/www/html/bootstrap/cache

# Set correct permission
RUN chmod -R 755 /var/www/html/storage
RUN chmod -R 755 /var/www/html/storage/logs
RUN chmod -R 755 /var/www/html/bootstrap

# Expose port
EXPOSE 9000

# Start PHP-FPM server
CMD ["php-fpm"]
